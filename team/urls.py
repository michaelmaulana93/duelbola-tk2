from django.contrib import admin
from django.urls import path
from . import views

app_name = 'team'

urlpatterns = [
    path('', views.team, name="team"),
    path('feedback/', views.get_Feedback, name="get_Feedback")
]
