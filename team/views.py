from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from .models import Feedback
from . import forms

def team(request):
    if request.method == "POST" :
        form = forms.FeedbackForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('team:team')


    else:
        form = forms.FeedbackForm()
    return render (request, 'team.html', {"form":form})

def get_Feedback(request):
    result = list(Feedback.objects.all().values())
    return JsonResponse({"data": result})
