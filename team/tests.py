from django.test import TestCase
from django.test import Client
from django.urls import reverse, resolve
from .views import *;
from team.forms import FeedbackForm


class UnitTest(TestCase):

    def setUp(self):
        self.client = Client()
        self.team = reverse('team:team')
        self.project = Feedback.objects.create(
            feedback='Great Website'
        )
        self.project1 = Feedback.objects.create(
            feedback='Great Website'
        )

    def test_url_doesnt_exist(self):
        response = self.client.get('/oii')
        self.assertEqual(response.status_code, 404)

    def test_url_exist(self):
        response = self.client.get(self.team)
        self.assertEqual(response.status_code, 200)

    def test_page(self):
        response = self.client.get(self.team)
        self.assertTemplateUsed(response, "team.html")

    def test_form_valid(self):

        form = FeedbackForm(data = {
        'feedback': 'Great Website'
        } )

        self.assertTrue(form.is_valid())

    def test_no_data(self):
        form = FeedbackForm(data={})

        self.assertFalse(form.is_valid())
        self.assertEquals(len(form.errors), 1)

    def test_team_GET(self):
        response = self.client.get(self.team, { "feedback": Feedback.objects.all()})
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'team.html')

    def test_team_POST_feedback(self):
        response = self.client.post(self.team,  { "feedback": Feedback.objects.all()})
        self.assertEquals(response.status_code, 302)

    def test_project(self):
        self.assertNotEqual(self.project, self.project1 )
