from django.db import models
from django.contrib.auth.models import User

class Post(models.Model):
    location = models.CharField(max_length=500)
    fieldType = models.CharField(max_length=500)
    operationHours = models.CharField(max_length=500)
    price = models.CharField(max_length=500)

    @property
    def isNumber(self):
    	return self.price > 0