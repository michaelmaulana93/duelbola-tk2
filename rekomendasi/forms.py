from django import forms
from rekomendasi.models import Post

class FindForm(forms.ModelForm):
    location = forms.CharField(label = 'Location:',
                                widget=forms.TextInput(attrs={"placeholder": "Lokasi futsal, Ex: Depok"}))
    fieldType = forms.CharField(label = 'Field Type:',
                                widget=forms.TextInput(attrs={"placeholder": "Jenis lapangan"}))
    operationHours = forms.CharField(label = 'Operation Hours:',
                                widget=forms.TextInput(attrs={"placeholder": "Jam buka"}))
    price = forms.CharField(label = 'Price:',
                                widget=forms.TextInput(attrs={"placeholder": "Harga per jam"}))

    class Meta:
        model = Post
        fields =(
            'location',
            'fieldType',
            'operationHours',
            'price',
        )