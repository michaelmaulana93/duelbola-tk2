from django.urls import path
from . import views

app_name = 'rekomendasi'

urlpatterns = [
    path('', views.LocationView.as_view(), name='rekomendasi'),
    # dilanjutkan ...
]

