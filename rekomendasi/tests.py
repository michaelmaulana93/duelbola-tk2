from django.test import TestCase
from django.urls import resolve, reverse
from django.utils import timezone
from . import views
from django.http import HttpRequest
from .forms import FindForm
from .views import LocationView
from .models import Post


from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions

# Create your tests here.
class UnitTest(TestCase):

    #test if url '/rekomendasi' exist
    def test_url_exist(self):
        response = self.client.get('/rekomendasi/')
        self.assertEqual(response.status_code, 200)

    #test if url '/rekomendasi/' is using templates and static files
    def test_templates(self):
        response = self.client.get('/rekomendasi/')
        self.assertTemplateUsed(response, 'halamanrekomendasi.html')

    def test_model_created(self):
        Post.objects.create(location = "Jakarta")
        location_objs = Post.objects.all()
        self.assertEqual(len(location_objs), 1)
        
        the_obj = location_objs[0]
        self.assertEqual(the_obj.location, "Jakarta")

    def test_get_model(self):
        location_obj = Post.objects.create(location = "Jakarta")
        get_obj = Post.objects.get(location ="Jakarta")

        self.assertEqual(location_obj, get_obj)

    def test_page_contained_form(self):
            response = self.client.get('/rekomendasi/')
            self.assertContains(response, FindForm()['location'])






