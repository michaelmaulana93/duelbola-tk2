from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from rekomendasi.forms import FindForm
from django.contrib.auth.models import User
from rekomendasi.models import Post

class LocationView(TemplateView):
    template_name ='halamanrekomendasi.html'

    def get(self, request):
        Post.objects.all().delete()
        form = FindForm()
        posts = Post.objects.all()

        args = {'form': form, 'posts': posts}
        return render(request, self.template_name, args)

    def post(self, request):
        form = FindForm(request.POST)
        if form.is_valid():

            text = form.cleaned_data['location']

            print(text)
            if(text == "Depok"):
                tag = "Pro Arena Futsal"
                field = "Field Type: Sintetis"
                hours = "Operation hours: 08.00 - 21.00"
                price = "Price: 150k/hour"
                image = "tempatfutsal.jpg"

                form.save()
            else:
                tag = "Sorry, no recommendation found in " + text + " area"
                field = ""
                hours = ""
                price = ""
                image = ""
                form.save()

            #text = form.cleaned_data[post]
            form = FindForm()
            #return redirect('template:location')


        args = {'form': form, 'text': text, 'tag':tag, 'field':field, 'hours':hours, 'price':price, 'image':image}
        return render(request, self.template_name, args)
