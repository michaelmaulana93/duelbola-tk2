from django.test import TestCase
from django.urls import resolve
from forum.views import index, post
from forum.models import Post
from forum.forms import PostForm
from django.contrib.auth.models import User

class ForumTest(TestCase):

    def test_forum_url_is_exist(self):
        response = self.client.get('/forum/')
        self.assertEqual(response.status_code, 200)

    def test_forum_using_forum_template(self):
        response = self.client.get('/forum/')
        self.assertTemplateUsed(response, 'forum/forum.html')
    
    def test_forum_using_index_func(self):
        found = resolve('/forum/')
        self.assertEqual(found.func, index)

    def test_post_forum_url_is_exist(self):
        response = self.client.get('/forum/post/')
        self.assertEqual(response.status_code, 302)

    def test_post_forum_using_post_forum_template(self):
        NewUser = User.objects.create_user(
            username='luqman',
            password= '123123',
            first_name='luq',
            last_name='man'
        )
        NewUser.save()
        self.client.post('/login/', {"username":'luqman', 'password':'123123', 'bebas':'biar panjangnya 3'})
        response = self.client.get('/forum/post/')
        self.assertTemplateUsed(response, 'forum/post_forum.html')
    
    def test_post_forum_using_post_func(self):
        found = resolve('/forum/post/')
        self.assertEqual(found.func, post)

    def test_model_can_create_new_post(self):
        new_post = Post.objects.create(post="Ada yang mau main bola?", kategori="General", user="Luqman")

        count_all_post = Post.objects.all().count()
        str(Post.objects.all().first())
        self.assertEqual(count_all_post, 1)
    
    def test_post_form_valid(self):
        form_data = {
            "post" : "Tim bola -1",
            "kategori" : "General",
        }
        post_form = PostForm(data = form_data)
        self.assertTrue(post_form.is_valid())

    def test_post_forum(self):
        NewUser = User.objects.create_user(
            username='luqman',
            password= '123123',
            first_name='luq',
            last_name='man'
        )
        NewUser.save()
        self.client.post('/login/', {"username":'luqman', 'password':'123123', 'bebas':'biar panjangnya 3'})

        response = self.client.post('/forum/post/',{"post":"Ayo main", "kategori":"General"})
        self.assertEqual(response.status_code, 302)
