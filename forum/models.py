from django.db import models
from django.utils import timezone

class Post(models.Model):
    user = models.CharField(max_length = 50)
    kategori = models.CharField(max_length = 15)
    post = models.TextField(max_length = 300)
    time_created = models.DateTimeField(default = timezone.now)

    def __str__(self):
        return self.user +" "+ str(self.time_created)