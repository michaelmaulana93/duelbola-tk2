from django.shortcuts import render, redirect
from forum.forms import PostForm
from forum.models import Post
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse

def index(request):
    return render(request, 'forum/forum.html')

@login_required(login_url='/login/')
def post(request):
    form = PostForm(request.POST or None)
    context = {
        'form' : form,
    }
    if request.method == "POST":
        if form.is_valid():
            data = request.POST.copy()
            form = PostForm(data)
            saved = form.save()
            saved.user = request.user.username
            saved.save()
            return redirect("forum:forum")
    return render(request, 'forum/post_forum.html', context)

def get_data_json(request):
    data = Post.objects.all()
    data_lst = []
    for i in data:
        dta = i.__dict__.copy()
        del dta["_state"]
        datetime = dta['time_created']
        date = datetime.date()
        time = str(datetime.time())[:8]
        dta['date'] = date
        dta['time'] = time
        data_lst.append(dta)
    return JsonResponse(data_lst,safe=False)
    