from django import forms
from forum.models import Post

class PostForm(forms.ModelForm):
    kategori = forms.CharField(widget=forms.TextInput(
        attrs = {
            'type' : 'text'
                }
    ))
    post = forms.CharField(widget=forms.Textarea(
        attrs = {
            'type' : 'text'
                }
    ))

    class Meta:
        model = Post
        fields = "__all__"
        exclude = (
            "time_created",
            "user",
        )