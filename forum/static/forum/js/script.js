$(document).ready(function(){
    showPost();
    $("#welcome-user").hover(function(){
        $(this).css({
            "font-weight":"bold"
        });
    });
    $("#welcome-user").mouseleave(function(){
        $(this).css({
            "font-weight":"normal"
        });
    });
})

function showPost(){
    $.ajax({
        url:"/forum/json/",
        type: "GET",
        dataType: "json",
        success: function(result){
            const countItem = result.length;
            let post = "";
            for (let i = 0; i < countItem; i++) {
                const item = result[i];
                // bagian satu post
                post += `<div class="text-center border border-primary konten-forum" style="margin: 1.3em;">`;
                // bagian judul konten
                post += `<div class="konten-judul" style="margin: 1em;">`;
                post += "User: <b>" + item.user + "</b><br>";
                post += "Kategori: <b>" + item.kategori + "</b><br>";
                post += "Tanggal: " + item.date + "<br>";
                post += "Waktu Post: " + item.time + "<br>";
                post += `</div>`;
                // bagian isi post
                post += `<div class="konten-isi" style="margin-top: 1em;">`;
                post += item.post;
                post += `</div>`;
                // tutup bagian satu post
                post += `</div>`;
            }
            $("#post-forum").html(post);
            $(".konten-forum").hover(function(){
                $(this).css({
                    "background-color": "#ddd"
                })
            });
            $(".konten-forum").mouseleave(function(){
                $(this).css({
                    "background-color": "white"
                })
            });
        }
    })
}