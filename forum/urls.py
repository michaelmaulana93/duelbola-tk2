from django.contrib import admin
from django.urls import path
from forum.views import index, post, get_data_json

app_name = 'forum'

urlpatterns = [
    path('', index, name="forum"),
    path('post/', post, name="post"),
    path('json/', get_data_json, name="json"),
]