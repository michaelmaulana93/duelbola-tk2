# Generated by Django 2.2.5 on 2019-12-13 10:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('forum', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='kategori',
            field=models.CharField(default='General', max_length=15),
            preserve_default=False,
        ),
    ]
