from django.test import TestCase
from django.urls import resolve
from base.views import index

class LandingTest(TestCase):

    def test_landing_url_is_exist(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_landing_using_home_template(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'home.html')
    
    def test_landing_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)