from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User

def index(request):
    context = {
        'greeting' : 'Welcome To Our Website',

    }

    if request.user.is_authenticated:
        context['greeting'] = 'Welcome ' + request.session["username"]

    return render(request, 'home.html', context)
