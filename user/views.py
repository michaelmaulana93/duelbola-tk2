from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User


# Create your views here.
def loginView(request):

    if request.method == "GET":
        if request.user.is_authenticated:
            return redirect('base:home')
        else:
            return render(request, 'login.html')

    if request.method == "POST":
        if len(request.POST) == 3:
            username = request.POST["username"]
            password = request.POST["password"]



            user = authenticate(username=username, password=password)



            if user is not None:
                login(request, user)
                request.session["username"] = user.username
                request.session["greeting"] = "Welcome To Our Website "+ user.username
                return redirect('base:home')
            else:
                return redirect('user:login')

        elif len(request.POST) == 4:
            username = request.POST['username']
            email = request.POST['email']
            password = request.POST['password']

            user = User.objects.create_user(username=username, email=email, password=password)
            login(request, user)
            request.session["username"] = user.username
            request.session["greeting"] = "Welcome To Our Website "+ user.username
            return redirect('base:home')


    # print(request.user)
    #
    # username_1 = "saya"
    # password_1 = "saya"
    # user = authenticate(request, username=username_1, password=password_1)
    # login(request, user)


@login_required
def logoutView(request):
    request.session.flush()
    if request.method == "GET":

        if request.user.is_authenticated:
            logout(request)
        return redirect('base:home')

    # print(request.user)
    #
    # username_1 = "saya"
    # password_1 = "saya"
    # user = authenticate(request, username=username_1, password=password_1)
    # login(request, user)
