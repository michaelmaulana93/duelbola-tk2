from django.test import TestCase
from django.test import Client
from django.urls import reverse, resolve
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium.webdriver.chrome.options import Options
from .views import *
import time
from selenium import webdriver

class UnitTest(TestCase):

    def setUp(self):

        self.user = reverse("user:login")

    def test_url_exist(self):
        response = Client().get(self.user)
        self.assertEqual(response.status_code, 200)

    def test_url_not_exist(self):
        response = Client().get("/masuk")
        self.assertEqual(response.status_code, 404)

    def test_page(self):
        response = Client().get(self.user)
        self.assertTemplateUsed(response, "login.html")

class TestFunctionalPage(StaticLiveServerTestCase):

    def setUp(self):

        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(TestFunctionalPage, self).setUp()

    def tearDown(self):

        self.browser.quit()
        super(TestFunctionalPage, self).tearDown()

    def test_can_login(self):
        # Create user
        User.objects.create_user(username='najib', email='najib@mail.com', password='test123')

        self.browser.get(self.live_server_url + "/login")

        username = self.browser.find_element_by_id('username_login')
        password = self.browser.find_element_by_id('password_login')
        submit = self.browser.find_element_by_id('submit_login')

        username.send_keys('najib')
        password.send_keys('test123')
        submit.click()
        time.sleep(10)

        self.assertIn("Welcome najib", self.browser.page_source)
        time.sleep(10)

    def test_can_signup(self):
        self.browser.get(self.live_server_url + "/login")

        title = self.browser.find_element_by_id('title_signup')
        username = self.browser.find_element_by_id('username_signup')
        email = self.browser.find_element_by_id('email_signup')
        password = self.browser.find_element_by_id('password_signup')
        submit = self.browser.find_element_by_id('submit_signup')

        title.click()
        username.send_keys('najib')
        email.send_keys('najib@mail.com')
        password.send_keys('test123')
        submit.click()
        time.sleep(10)

        self.assertIn("Welcome najib", self.browser.page_source)
        time.sleep(10)

    def test_auth_can_access_logout(self):
        # Create user
        User.objects.create_user(username='najib', email='najib@mail.com', password='test123')

        self.browser.get(self.live_server_url + "/login/")

        username = self.browser.find_element_by_id('username_login')
        password = self.browser.find_element_by_id('password_login')
        submit = self.browser.find_element_by_id('submit_login')

        username.send_keys('najib')
        password.send_keys('test123')
        submit.click()
        time.sleep(10)

        logout = self.browser.find_element_by_id('logout')
        logout.click()
        time.sleep(10)

        self.assertIn("D U E L", self.browser.page_source)
        time.sleep(10)

    def test_login_with_wrong_username(self):
       self.browser.get(self.live_server_url + "/login")



       username = self.browser.find_element_by_id('username_login')
       password = self.browser.find_element_by_id('password_login')
       submit = self.browser.find_element_by_id('submit_login')

       username.send_keys('najib')
       password.send_keys('test123')
       submit.click()
       time.sleep(10)
       self.authpage_login = reverse("user:login")
       response4 = Client().get(self.authpage_login)



       self.assertTemplateUsed(response4, 'login.html')
       time.sleep(10)

    def test_signup_with_same_username(self):
        # Create user
        User.objects.create_user(username='najib', email='najib@mail.com', password='test123')

        self.browser.get(self.live_server_url + "/login")



        title = self.browser.find_element_by_id('title_signup')
        username = self.browser.find_element_by_id('username_signup')
        email = self.browser.find_element_by_id('email_signup')
        password = self.browser.find_element_by_id('password_signup')
        submit = self.browser.find_element_by_id('submit_signup')

        title.click()
        username.send_keys('najib')
        email.send_keys('najib@mail.com')
        password.send_keys('test123')
        submit.click()
        time.sleep(10)
        self.authpage_login = reverse("user:login")
        response4 = Client().get(self.authpage_login)



        self.assertTemplateUsed(response4, 'login.html')

        time.sleep(10)

    def test_auth_cannot_access_login(self):
        # Create user
        User.objects.create_user(username='najib', email='najib@mail.com', password='test123')

        self.browser.get(self.live_server_url + "/login")

        username = self.browser.find_element_by_id('username_login')
        password = self.browser.find_element_by_id('password_login')
        submit = self.browser.find_element_by_id('submit_login')

        username.send_keys('najib')
        password.send_keys('test123')
        submit.click()
        time.sleep(10)

        self.browser.get(self.live_server_url + "/login")

        self.assertIn("Welcome najib", self.browser.page_source)
        time.sleep(10)
